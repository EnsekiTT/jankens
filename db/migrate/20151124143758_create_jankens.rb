class CreateJankens < ActiveRecord::Migration
  def change
    create_table :jankens do |t|
      t.string :user
      t.string :enemy
      t.integer :userhand
      t.integer :enemyhand
      t.integer :judge

      t.timestamps null: false
    end
  end
end
