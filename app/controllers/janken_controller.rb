class JankenController < ApplicationController
  def tweet_it
    return true
  end
  
  def input
    if session[:url] != nil
      redirect_url = session[:url]
      session[:url] = nil
      redirect_to redirect_url
    end
  end
  
  def battle
    # Battle データの読み込み
    temp_janken = Janken.find_by_id(params[:id])
    @user = temp_janken.user
    @enemy = temp_janken.enemy
    @id = params["id"]
    if !signed_in?
      session[:url] = request.base_url() + '/jankens/' + @id.to_s
    end
  end

  def update
    if signed_in?
      enemy = params[:enemyname]
      userhand = params[:userhand]

      # Modelオブジェクトに登録する
      @janken_id = nil
      Janken.new do |janken|
        janken.user = session[:username]
        janken.enemy = enemy
        janken.userhand = userhand
        janken.enemyhand = 0
        janken.judge = 0
        janken.save
        @janken_id = janken.id
      end
      
      # Tweetを送る
      client = Twitter::REST::Client.new do |config|
        config.consumer_key = ENV["CONSUMER_KEY"]
        config.consumer_secret = ENV["CONSUMER_SECRET"] 
        config.access_token = session[:oauth_token]
        config.access_token_secret = session[:oauth_token_secret]
      end
      uri = request.base_url() + '/jankens/' + @janken_id.to_s
      tweet = "@" + enemy + ' じゃんけん勝負が申し込まれました ' + uri + " #ついったぽい"
      if tweet_it == true
        client.update(tweet)
      else
        print tweet
      end
      @result = :success
    else
      session[:enemyname] = params[:enemyname]
      session[:userhand] = params[:userhand]
      @result = :not_signed_in
      redirect_to request.base_url + '/auth/twitter'
    end
  end
  
  def update_battle
    # Battle データの読み込み
    temp_janken = Janken.find_by_id(params[:id])
    @url = request.base_url() + '/jankens/' + params[:id].to_s
    @user = temp_janken.user
    @enemy = temp_janken.enemy
    print @enemy
    if signed_in?
      if @enemy.strip.downcase != session[:username].strip.downcase
        redirect_to request.base_url
      end
      session[:url] = nil
      enemy = true
      # 勝敗情報を登録する
      result = judge_func(temp_janken.userhand, params[:enemyhand])
      temp_janken.enemyhand = params[:enemyhand]
      temp_janken.judge = result
      temp_janken.save
      
      # Tweetを送る
      client = Twitter::REST::Client.new do |config|
        config.consumer_key = ENV["CONSUMER_KEY"]
        config.consumer_secret = ENV["CONSUMER_SECRET"] 
        config.access_token = session[:oauth_token]
        config.access_token_secret = session[:oauth_token_secret]
      end
      
      if result == 0
        message = ' この勝負、あいこ '
      elsif result == 1
        message = ' この勝負、@' + @enemy + ' の勝ち。 '
      elsif result == 2
        message = ' この勝負、@' + @user + ' の勝ち。 ' 
      else
        message = ' この勝負、なにかおかしいぞ？ '
      end
      uri = request.base_url()
      tweet = "@" + @user + ' VS @' + @enemy + message + uri + " #ついったぽい"
      if tweet_it == true
        client.update(tweet)
      else
        print tweet
      end
      
      @result = :success
    else
      session[:url] = @url
      @result = :not_signed_in
      redirect_to request.base_url + '/auth/twitter'
    end
  end
  
  def judge_func(user, enemy)
    # user is => win:2 lose:1 even:0
    print user
    print enemy
    judge_param = (user.to_i - enemy.to_i + 3) % 3
    return judge_param
  end
  
end
