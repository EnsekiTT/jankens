require 'test_helper'

class JankenControllerTest < ActionController::TestCase
  test "should get input" do
    get :input
    assert_response :success
  end

  test "should get update" do
    get :update
    assert_response :success
  end

  test "should get battle" do
    get :battle
    assert_response :success
  end

end
